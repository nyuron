module Color
	@base = default, default
	@left_side = blue, default, bold
	@priority = blue, default
	module Normal
	end

	module Selected
		@base = blue, white, reverse
		@content = blue, black, reverse
	end

	module Marked
		@base = cyan, black, reverse
#		@content = cyan, black, reverse
	end

	module Done
		@content = blue, default
	end

	module Console
		@base = default, default, none
		@date = blue, default, bold
		@tags = blue, default, bold
		@info = blue, default, bold
		@error = red, default, bold
	end
end

