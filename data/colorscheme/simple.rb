module Color
	@terminal_cursor = true
	@priority = magenta, default, none

	module Selected
		@base = cyan, default, reverse
	end

	module Marked
		@base = red, default, none
	end

	module Done
		@content = green, default, none
	end
end

