module Color
	@base              = default, default, none
	@priority_positive = green,   default
	@priority_negative = red,     default
	@tags              = default, default, bold

	@txt               = default, default, bold
	@tag               = green,   default, bold
	@opt               = yellow,  default, bold
	@cmd               = blue,    default, bold
	@map               = blue,    default, bold

	module Normal
		@content   = default, default
	end

	module Selected
		@base      = cyan,    black,   reverse
	end

	module Marked
		@base      = red,     black,   reverse
	end

	module Done
		@content   = green,   default, none
	end

	module Console
		@tags = default, default, bold
		@date = default, default, none
		@error = red, default, none
	end
end

