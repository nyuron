#!/usr/bin/env ruby
require 'pp'
$data = {}

$adr1 = nil
$adr2 = nil

## some definitions {{{
def current() $data[$adr1] end

def get()  current[$adr2] end

def set(x) current[$adr2] = x end

def found(key)
	return false if $cleared
	!$db.get_first_row("SELECT * FROM nyu WHERE type='cmd' AND key=?;", key).nil?
end

def update(key, data)
	$db.execute("UPDATE nyu SET mtime=?, value=?, descr=?, info1=? WHERE type='cmd' AND key=?;", Time.now.to_i, data['value'], data['descr'], data['tab'], key)
end

def create(key, data)
	$db.execute("INSERT INTO nyu (key,type,ctime,mtime,value,descr,info1) VALUES (?, 'cmd', ?,?,?,?,?);", key, Time.now.to_i, Time.now.to_i, data['value'], data['descr'], data['tab'])
end
## }}}

## reading the data about commands from command.rb
print 'reading '
file = File.join(File.dirname(__FILE__), 'commands.rb')
File.read(file).each_line do |line|
	if line =~ /^#\s*([\w\d]+)(?:\s*\.\s*([\w\d]+))?(?:\s*:\s*(.+))?/
		next if current and get.empty?
		$adr1 = $1
		$adr2 = $2 || 'value'
		print $adr2[0].chr
		$data[$adr1] ||= {}
		current['descr'] = $3 if $3
		set ''
		next
	end

	if current
		print '.'
		get << line
	end
end

puts 'done!'

## asking what to do
while true
	puts
	puts 'current path: ' + Dir.pwd + '/'
	print 'write to which file? (leave free to print) '
	gets

	if $_.strip!.empty?
		pp $data
		exit
	end

	file = File.expand_path($_.strip)

	while true
		puts "file = #{file.inspect}"
		print "is that correct? (yes/no) "
		gets
		break if %w[yes no].include? $_.strip!
	end

	break if $_ == 'yes'
end

## if okay, write stuff to db
while true
	print "clear old commands before applying new ones? (clear/no) "
	break if %w[clear no].include? gets.strip!
end

require 'sqlite3'
$db = db = SQLite3::Database.new(file)

if $cleared = ($_ == 'clear')
	db.execute "DELETE FROM nyu WHERE type='cmd'"
end

db.transaction

for key, val in $data
	if found(key)
		puts " ) #{key} found. updating."
		update key, val
	else
		puts "+) #{key} not found. creating."
		create key, val
	end
end
db.commit

puts "done."

