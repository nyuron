# at:sets the time of the current note to <arg1>
for it in them
	it.time = Convert.string_to_time(rest, it.time)
end

# in:sets the time of this note, relative to now
for it in them
	it.time = Convert.string_to_relative_time(rest, it.time, 1)
end

# ago:sets the time of this note, relative to now
for it in them
	it.time = Convert.string_to_relative_time(rest, it.time, -1)
end

# chain:splits <rest> at | and treats each section as a seperate command
for cmd in rest.split('|')
	call(cmd)
end

# console:opens the console with an optional string
Console.open(rest)

# delete:deletes the currently selected neuron
delete them
refilter!

# dumpoptions:logs the Options
logpp Opt

# dumpsort:logs the sorting code
log Convert.sort_sequence_to_code(Opt.sort)

# edit:opens the current neuron in an external editor
if rest.empty?
	edit 'value'
else
	edit rest
end

# edit.tab
tab VALID_EDIT_KEYWORDS

# enter:either edits the neuron or, if its a tag, filter it.
if this.tag? and this.key?
	Opt.filter = this.key
else
	edit 'value'
end

# eval:evaluates ruby code
eval rest

# fail:divides by zero
1/0

# goto
goto arg1.to_i

# insert
it = new 'txt', nil, rest0
it.tags = this.tags
it.prio = this.prio

refilter
resort
find_later it

if Opt.filter.include? '@time'
	it.time = Time.now
	Console.open "at "
end

# inspect
string = this.inspect.nolinebreaks
log string
Console.write string

# map
nyu = Neuron.find('map', arg1)
if nyu == EmptyNeuron
	find new 'map', arg1, rest1
else
	nyu.value = rest1
	find nyu
end

# map.tab
tab Cache.map.__table__.keys

# mark
if nyu = Neuron.find 'map', arg1
	nyu.value = "set filter #{Opt.filter}"
end

# move
move rest.to_i

# move_percent
move lines / 100.0 * rest.to_f

# new
find new(arg1, arg2, rest2)

# prio_add
old = them.first
them.all.prio_add(arg1.to_i)
resort
find_later old

# prio_set
old = them.first
them.all.prio = arg1.to_i
resort
find_later old

# put
case arg1
when 'filter'
	Console.open "set filter #{Opt.filter}"
when 'tags'
	Console.open "tag #{Convert.tags_to_string(this.tags)}"
when 'new'
	if this.text?
		Console.open "insert "
	else
		Console.open "new "
	end
end

# quit
exit

# refilter
refilter!

# reoc
for it in them.select { |x| x.is_text? }
	it.info2 = rest
	it.tags_add 'r'
end

# run
system(rest)

# search_backward
Console.open(rest, :searchback)

# search_forward
Console.open(rest, :search)

# search_next
scan :next

# search_prev
scan :prev

# set
Opt[word1] = rest1

# sync
Cache.sync!
sync_tags
refilter!

# tag
them.all.tags = rest
for tag in Convert.string_to_tags(rest)
	Query.manage_tag(tag)
end
them.all.update

# tagadd
them.all.tags_add *@words[1..-1]

# tagrem
them.all.tags_remove *@words[1..-1]

# tagremall
UI.list.all.tags_remove *@words[1..-1]

# tagtoggle
them.all.tags_toggle *@words[1..-1]

# colorscheme:loads a colorscheme from data/colorscheme/
Color.load_colorscheme(rest)

# colorscheme.tab
tab Dir[File.join(MYDIR, 'data', 'colorscheme', '*.rb')].map! {|x| File.basename(x)[0...-3] }

# p
logpp eval rest

# filter
Opt.filter = rest

# filter.tab
tab Query.get_all_tags

# toggle:toggles an option (must be of type boolean (bol))
if Opt[arg1] == false
	Opt[arg1] = true
elsif Opt[arg1] == true
	Opt[arg1] = false
end

# opt_add:adds a number to an option, which may be negative.
if Opt[arg1].is_a? Numeric
	Opt[arg1] += arg2.to_i
end
