#!/usr/bin/env ruby
## Nyuron: dynamic notepad & calendar
version = '4.1.4'


## boot up --------------------------------------

## find the path of this script
require 'pathname'
MYDIR = File.dirname(Pathname.new(__FILE__).realpath)
$LOAD_PATH << MYDIR

require 'modules/boot'
include Boot


## check arguments

if ARGV.include? '-v' or ARGV.include? '--version'
	abort "nyuron #{version}"
elsif ARGV.include? '--help'
	abort "usage: nyuron [--version] [--help]"
end


## set up & load --------------------------------

## find & run rc file
fname = get_conf_path

## predefine some variables to set their scope
dbfile, editor, loglevel, logfile, colorscheme, stfu = nil

load fname if fname

unless stfu or ("1.8"..."1.9").include? RUBY_VERSION
	puts "note that this program was designed for Ruby 1.8.7"
	puts "you are running: #{RUBY_VERSION}, so you might encounter some errors."
	puts "write stfu = true into your .nyurc to supress this message"
end

remember = dbfile
dbfile, places = get_database_path_and_places(dbfile)

unless dbfile
	puts "error: I can't find a database. without it I cannot run,"
	puts "since all commands are stored there. I searched at this places:"
	puts places.select {|x| x.is_a? String}
	puts
	puts "if you have a database, please put it there. if not, download the"
	puts "newest Nyuron release and copy the default database from there."
	exit
end


## require all files in modules/ and classes/
cant_find = []
for file in Dir[File.join(MYDIR, '{modules,classes}', '**', '*.rb')]
	begin
		require file [MYDIR.size + 1..-4]
	rescue LoadError
		cant_find << $!.backtrace.join("\n")
	end
end

unless cant_find.empty?
	puts "While I was loading the required files, i ran into these errors:"
	cant_find.each {|x| puts x}
	puts
	puts "Please make sure you have ruby-sqlite3 and ruby-ncurses installed"
	puts "and inside of one of those directories:"
	puts
	$LOAD_PATH.each {|x| puts x}
	exit
end

## require colorscheme
colorscheme = find_first_existing_file(
	File.join(MYDIR, "data", "colorscheme", "#{colorscheme}.rb"),
	File.join(MYDIR, "data", "colorscheme", "snow.rb")
)

load colorscheme if colorscheme

include Debug

## initialize the Info object which contains basic information
Info = OpenStruct.new()
Info.editor = (editor || "vim '+set ft=ruby' '+set noendofline' '+map q ZZ' '%s'")
Info.loglevel = (loglevel || 1)
Info.logfile = File.expand_path(logfile || '/tmp/errorlog')
Info.dbfile = dbfile

Info.version = version
Info.where = :main
Info.request_draw = true
Info.request_filter = true
Info.request_sort = true

debug_log_stream = File.open(Info.logfile, 'a')
debug_log_stream.sync = true
Debug.setup(
	:name => 'nyuron',
	:stream => debug_log_stream,
	:level => Info.loglevel
)

## Initialize Cache object, which caches parts of the database
## for quicker access
Cache = CacheClass.new( *%w(cmd map opt) )
Opt = Cache.opt


## let's nyu! -----------------------------------
begin
	$now = Time.now
	CLI.initialize
	Console.initialize
	SQL.initialize
	UserInterface.meta_loop

rescue SystemExit, Interrupt
	## quietly ignore

rescue Exception
	logwrite_fatal "Fatal exception: "
	lograise
	raise

ensure
	log "---- the end ----"
	UserInterface.close
end

