class Command
	attr_accessor(*%w(str words rests name code))

	def initialize(string)
		str = string.lstrip
		@string = str.dup
		@words = []
		@rests = []

		begin
			word, str = str.split(' ', 2)
			@words << word
			@rests << (str || '')
		end while str and !str.empty?

		@name = words.first

		@code = Cache.cmd[@name]
		unless @code
			Console.error "Command `#{@name}' not found!"
			return false
		end

		@env = Environment.new
		@env.words = @words
		@env.rests = @rests
		@env.string = @string
		@env.name = @name
		@env.key = ''
	end

	def run_code(keybuffer=nil)
		if Environment === @env
			if keybuffer.is_a? String
				@env.key = keybuffer
			end
			@env.run @code
		end
	end

	def tabcomp(cursor_pos, word_number)
		if Environment === @env
			@env.cursor_pos = cursor_pos
			@env.word_number = word_number
			@tabcomp = Query.get_tabcomp(@name).try.first
			if @tabcomp
				@env.run @tabcomp
			end
		end
	end

	## shortcut
	def self.run(string, keybuffer=nil)
		Command.new(string).run_code(keybuffer)
	end

	def self.tabcomp(string, cursor_pos, word_number)
		result = Command.new(string).tabcomp(cursor_pos, word_number)
		log "result: #{result.inspect}"
		unless Enumerable === result
			return []
		end
		return result
	end
end

