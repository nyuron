class CacheClass
	class Container
		def initialize(type)
			@type = type
			@table = {}
		end

		## redirects to either [] or []=
		def method_missing(a, val=nil)
			key = a.to_s
			if key[-1] == ?=
				key.slice!(-1)
	#			log "self[#{key}] = #{val}"
				self[key] = val
			else
				self[key]
			end
		end

		## getter for @table, to perform raw operations
		def __table__()
			@table
		end

		## getter
		def [](index)
			@table[index]
		end

		## setter
		##
		## Note: the @table value is not changed here but in Neuron#value=
		def []=(x, y)
			neuron = Neuron.from_query("type=? AND key=?", @type, x)
			if neuron.empty?
				neuron = Query.create_and_return(@type, x, y)
			end
			neuron.value = y
		end

		## API.sync(@type)
		def sync!
			API.sync(@type)
		end
	end
end
