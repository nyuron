## The purpose of this class is to create an abstract interface to datatypes
## like Options or Keymaps, and to allow the data to be changed
## * both in the database *and* in the buffer with one operation,
## * from a neuron object *or* the dictionary-like Cache-object
##   with the same result:
##
##     Cache.opt.foo = "bar"
##     Neuron.find('opt', 'foo').value = "bar"
##
## Why do I do this anyway?
##
## Sometimes in the code you will have to deal with a neuron object, eg. in a command,
## where API.this() is the currenly selected neuron.
##
## At other times you only have the key, eg. if you want to change a specific
## option. So you'll need some way to do any operation in both cases.
##
## Also, you will want to always have the data up to date without re-reading
## from the database before each operation. When using multiple clients, or
## changing the database with another program, the cache might become outdated
## and using sync! to synchronize it will be necessary.
class CacheClass
	## Create a new Cache object with the supplied types. a getter function
	## will be created for each type.
	def initialize(*types)
		@hash = {}
		for type in types
			eval "@hash[type.to_sym] = @#{type} = Container.new(type)"
			eval "def #{type}() @#{type} end"
		end
	end

	## Calls CacheClass::Container#sync! on all types.
	def sync!
		for key, type in @hash
			type.sync!
		end
	end

	## an alternative to cache.opt is cache[:opt] or cache['opt']
	def [](type)
		@hash[type.to_sym]
	end

	def add(neuron)
		return unless neuron.key
		container = self[neuron.type]
		return unless container
		return container.__table__[neuron.key] = neuron.value
	end

	def delete(neuron)
		return unless neuron.key
		container = self[neuron.type]
		return unless container
		log neuron.type
		log neuron.key
		container.__table__.delete(neuron.key)
	end

	alias << add
	alias >> delete
end

#c = Cach3.new
#c.opt.bla = 5
#puts c.opt.bla
