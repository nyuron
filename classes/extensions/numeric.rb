class Numeric
	def between?(a, b)
		self >= a and self <= b
	end

	def ord
		self
	end

	def suffix
		case self % 10
		when 1; 'st'
		when 2; 'nd'
		when 3; 'rd'
		else 'th' end
	end

	## TODO: i think it doesn't quite work with negative numbers

	## keeps a number inside a defined range and wraps it around
	## if it steps over the borders. accepts 1-2 integers or a range.
	##  for n in 0..8
	##    print n.wrap(4), ', '
	##  end
	##  # returns:
	##  0, 1, 2, 3, 0, 1, 2, 3, 0, 
	def wrap(max, min = 0)
		## if the argument is a range, use it :)
		if max.is_a? Range
			min = max.min
			max = max.max
		end

		## swap min and max if min is larger than max
		if min > max
			min, max = max, min
		end

		## if min is 0, this is the equivalent of a modulo.
		## note that ruby's modulo wraps on negative
		## numbers :) so there's nothing else to do.
		if min == 0
			return self % max
		end
		
		## otherwise, substract min before, and add min after
		## the modulo operation to get the correct result.
		return (self - min) % max + min
	end

	## Units
	def second()          1 * self end
	def minute()         60 * self end
	def   hour()       3600 * self end
	def    day()      86400 * self end
	def   week()     604800 * self end
	def  month()    2629800 * self end
	def   year()   31557600 * self end

	alias   seconds   second
	alias   minutes   minute
	alias     hours     hour
	alias      days      day
	alias     weeks     week
	alias    months    month
	alias     years     year
end

### generate the unit methods {{{
#values = {
#  'second' => 1,
#	'minute' => 60,
#	'hour' => 3600,
#	'day' => 3600 * 24,
#	'week' => 3600 * 24 * 7,
#	'month' => 3600 * 24 * 30.4375,
#	'year' => 3600 * 24 * 365.25,
#}
#values.each do |key, val|
#	puts "	def %6s() %10d * self end" % [key, val, key, key]
#end
#values.each do |key, val|
#	puts "	alias %8ss %8s" % [key, key]
#end
### }}}

