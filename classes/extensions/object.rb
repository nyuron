class Void; end

## applies a method if possible, otherwise returns nil
##    "foo".try.upcase => "FOO"
##      nil.try.upcase   => nil # and *no* error
class Object
	def try(*args)
		if args.empty?
			Try.new(self)
		else
			send(*args) rescue nil
		end
	end

	def in(*args)
		args.include? self
	end
	alias in? in

	alias is ==

	alias is_an? is_a?

	def all() self end

	## This class is used in Object#try
	class Try < Void
		def initialize(obj)
			@obj = obj
		end
		def method_missing(name, *args)
			return @obj.send(name) rescue nil
		end
	end
end

