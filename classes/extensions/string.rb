class String
	## str.gsubalot([a,b], [c,d])
	## has the same result as
	## str.gsub(a,b).gsub(c,d)
	def gsubalot(*pairs)
		result = self.dup
		for from, to in pairs
			result.gsub!(from, to)
		end
		return result
	end

	def nolinebreaks
		result = dup
		## replace "\n" with ";  " unless the following line is empty
		result.gsub!(/\n(?=[^\n]*[\w\d])/, ";  ")
		## delete all other "\n"s
		result.gsub!("\n","")
		## replace tabs with spaces
		result.gsub!("\t", " ")
		result
	end

	def is_number?
		strip =~ /^[-+]?(\d+|\d*(\.\d*))$/
	end

	def is_integer?
		strip =~ /^[-+]?\d+$/
	end

	def is_word?
		self =~ /[A-Za-z]/
	end

	## escape
	def sql()
		gsubalot(
			['"', '\"']
		)
	end

	def clear() replace "" end
	alias clear! clear

	## i want no error if n < 0
	alias old_multiply *
	def *(n)
		if n > 0 then
			old_multiply(n)
		else
			""
		end
	end
end
