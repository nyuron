class Float
	if RUBY_VERSION < "1.9"
		alias __old_round__ round
		## define round as it is in ruby1.9.
		## the argument specifies the number of digits after zero
		def round(x=0)
			return self.to_i if x == 0

			factor = 10 ** x
			(self * factor).__old_round__.to_f / factor
		end
	end
end
