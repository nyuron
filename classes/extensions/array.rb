class Array
	##  [1,2,3,4].combine([7,8,9]) => [[1,7], [2,8], [3,9], [4,nil]]
	##  [1,2].combine([7,8,9]) => [[1,7], [2,8]]
	def combine!(other)
		ix = -1
		self.map! do |val|
			ix += 1
			[val, other[ix]]
		end
		self
	end

	## self.dup.combine!
	def combine(other) dup.combine!(other) end

	## sometimes more efficient than select{...}.first
	##   [100, 50, 20, 10].select_first {|x| x < 50} # => 20
	def select_first(&block)
		for item in self
			if block.call(item)
				return item
			end
		end
		return nil
	end

	## works like select_fist but returns the index, not the item.
	def index_with_block(&block)
		each_with_index do |item, index|
			if block.call(item)
				return index
			end
		end
		return nil
	end

	## counts how often obj occurs in the array.
	def count(obj)
		select {|x| x == obj}.size
	end
end
