## this Object is empty and has no methods whatsoever
class Void
	oldv, $-v = $-v, nil

	for method in instance_methods
		remove_method(method) rescue nil
	end

	$-v = oldv
end
