## This is used as a dummy in situations where you count
## on having a Neuron object but there is no real one available
module EmptyNeuron
	extend EmptyNeuron
	def empty?() true end
	def type() 'txt' end
	def rowid() nil end
	def ctime() 0 end
	def mtime() 0 end
	def prio() 0 end
	def tags() Convert.filter_to_tags(Opt.filter) end
	def value() "" end
	def raw_value() "" end
	def sortstring() "" end
	def time() false end
	def bold_on_the_left() false end
	def text?() true end
	def option?() false end
	def keymap?() false end
	def command?() false end
	def time?() false end
	def this() self end
	def method_missing(*args)
		nil
	end
end
