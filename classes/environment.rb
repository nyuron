## An Environment is used when executing commands. It provides short ways of accessing
## information. For example:
## * arg returns the string inserted after the command.
## * name returns the name of the command
## * rest2 returns the rest after the second argument.
##
##  env = Environment.new
##  env.string = "test"
##  code = "print(@string)"
##  env.run(code) ## prints the string "test"

class Environment
	include API

	attr_accessor(*%w(
		string words rests name
		word_number cursor_pos key
	))

	## creates a new, empty environment.
	def initialize
		@args = @words = []
		@rests = []
	end

	## before running code, it's a good idea to use the attribute accessors to set
	## environmental variables. these are:
	## * name=
	## * string=
	## * words=
	## * rests=
	## * key=
	## * word_number=
	## * cursor_pos=
	def run(code)
		begin
			eval(code)
		rescue StandardError, ScriptError
			Console.raise
		end
	end

	## returns the rest after the word n. methods with the name
	## restN with N from 0 to 15 are generated for more comfortable use.
	##
	## It's a bit confusing where the counting begins, so I add an example:
	##
	## if the command is "set filter salt & pepper"
	## * rest(0) => "filter salt & pepper"
	## * rest(1) => "salt & pepper"
	## * rest(2) => "& pepper"
	def rest(n=0)
		(@rests.is_a?(Array) && @rests[n]) || ""
	end

	## returns the nth space-seperated word. methods with the name
	## wordN for n from 0 to 15 are generated for more comfortable use.
	##
	##
	## It's a bit confusing where the counting begins, so I add an example:
	##
	## if the command is "set filter salt & pepper"
	## * word(0) => "set"
	## * word(1) => "filter"
	## * word(2) => "salt
	def word(n=0)
		(@words.is_a?(Array) && @words[n]) || ""
	end

	## arg is an alias for word: @words == @args, wordN == argN, word(N) == arg(N)
	alias arg word

	def at_word(n)
		if Range === n
			n === @word_number
		else
			@word_number == n
		end
	end

	for i in 0..16
		eval <<-DONE
			def word#{i}() word #{i} end
			def rest#{i}() rest #{i} end
			alias arg#{i} word#{i}
		DONE
	end
end

