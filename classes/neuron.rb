## classes/neuron

## A description of the attributes:
##
##   @rowid (Integer) from 1 to the limit of the SQL database
##   contains an Integer with the Row ID of the neuron in the sql database.
##   
##   @key (String or nil)
##   contains the key, if it exists. Texts usually don't have a key.
##   
##   @type (String) one of { cmd map opt tag txt }
##   depending on the type, the neuron might have a different behaviour
##   
##   @ctime (Integer)
##   the creation time, as an integer time stamp. 0 if not found.
##   
##   @mtime (Integer)
##   the modification time, as an integer time stamp. 0 if not found.
##   
##   @prio (Integer or Float)
##   The priority, for sorting.
##   
##   @tags (Array<String>)
##   A list of strings, the tags that are assigned to this neuron.
##   
##   @value (may be any type)
##   The value is normally a string or nil. Options might use @info2 to specify
##   other types. The main content is here, eg text, command code, setting values
##   
##   @descr (String or nil)
##   If this is set, it might be shown instead of the usual "Type: key => value"
##   
##   @onchg (String or nil)
##   The code which is executed every time the value is changed via value=
##   
##   @sortstring (String)
##   The string by which it will be sorted when using the value keyword.
##   This is the same as @value, except for options, where it's the raw string.
##
##   @time (false or Time)
##   is false, unless @type is "txt" and @info1 is not nil
##
##   @bold_on_the_left, @bold_on_the_right (Integer)
##   how many characters from the left or from the right should be made bold?
##   @bold_on_the_left is usually 18,
##   @bold_on_the_right is the width of the string that indicates priority.
## 
##
## each neuron has generic info-attributes, which contain type-specific data.
## Here is a Guidemap. As you can see,
## there is still plenty of room for additions in the future.
##   Type | Long Name    | @info1               | @info2       |   @info3
##   -----+--------------+----------------------+--------------+------------
##   cmd  | Command      | Tabcompletion Code   |              |
##   map  | Keymap       |                      |              |
##   opt  | Option       | (used to be @onchg)  | Datatype(s)  |
##   tag  | Tag          |                      |              |
##   txt  | Text         | Deadline (timestamp) | Reoccur Code |
class Neuron
	include API

	## constructors {{{

	def initialize(table)
		@rowid, @key, @type, @ctime, @mtime, @prio, @tags,
			@value, @descr, @onchg, @info1, @info2, @info3 = table
		
		## default values
		@prio = @prio ? @prio.to_i : 0
		@value = @value ? @value : ""
		@sortstring = @value
		@key = @key ? @key : ""
		@rowid = @rowid.to_i
		@mtime ||= 0
		@ctime ||= 0
		@tags = Convert.query_to_tags(@tags)
		@raw_value = @value
		@tagstring = nil

		if option?
			@value = Convert.query_to_option(value, @info2)
		end

		@time = false
		if text?
			if @tags.include? 'always_now'
				@info1 = $now.to_i - 1
				@time = Time.at(@info1)
			elsif @info1 and not @info1.empty?
				@info1 = @info1.to_i
				@time = Time.at(@info1)
				if @info2 and not @info2.empty? and @info1 < $now.to_i and @tags.include?('r')
					update_reoccur_option
				end
			end
		end

		@bold_on_the_left = 18
		@bold_on_the_right = 0
		@width = API.cols
	end

	def self.from_query(condition, *args)
		test = Query.get_where(condition, *args)
		test ? new(test) : EmptyNeuron
	end

	def self.from_rowid(n)
		from_query("rowid=?", n) || EmptyNeuron
	end

	def self.find(type, key)
		from_query("type=? AND key=?", type, key) || EmptyNeuron
	end

	## }}}
	## attributes and related stuff {{{

	PAD_WITH_TYPE = 10
	PAD_WITHOUT_TIME = 10
	PAD_WITH_TIME = 18

	TYPES_MAP = {
		"cmd" => "Command",
		"map" => "Keymap",
		"opt" => "Option",
		"tag" => "Tag",
		"txt" => "Text"
	}


	%w(key type ctime mtime prio descr onchg info1 info2 info3).each do |key|
		eval <<-DONE
			def #{key}() @#{key} end
			def #{key}=(x)
				reset
				@#{key} = x
				Query.set(self, '#{key}', x)
			end
		DONE
	end

	attr_reader(*%w(rowid tags value bold_on_the_left bold_on_the_right sortstring time raw_value tagstring))
	alias row_id rowid

	def empty?() false end

	def tags=(x)
		oldtags = @tags
		if Array === x
			@tags = x
		else
			@tags = Convert.string_to_tags(x)
		end
		@tags.uniq!
		if oldtags != @tags
			reset
			Query.set(self, 'tags', Convert.tags_to_query(@tags))
		end
		update_reoccur_option if @tags.include?('r')
		@tags
	end

	def update_reoccur_option
		if Opt.next_reoccur.nil?
			Query.create_opt("next_reoccur", @info1, "int")
		elsif Opt.next_reoccur.to_i > @info1 or Opt.next_reoccur.to_i == 0
			Opt.next_reoccur = @info1
		end
	end

	def prio_add(int)
		self.prio += int
	end

	def tags_add(*args)
		self.tags += args
		args
#		self.tags = (self.tags + args).uniq
	end

	def tags_remove(*args)
		self.tags -= args
	end

	def tags_toggle(*args)
		for arg in args
			if @tags.include? arg
				tags_remove arg
			else
				tags_add arg
			end
		end
	end

	def info1=(x)
		reset
		if text?
			case x
			when Time, Numeric
				@info1 = x.to_i
			when String
				if x.empty?
					@info1 = nil
				else
					@info1 = x.to_i
				end
			when NilClass
				@info1 = nil
			end
		else
			@info1 = x
		end
		Query.set(self, 'info1', @info1)
	end

	def info2=(x)
		if text?
			update_reoccur_option
		end
		Query.set(self, 'info2', (@info2 =x))
	end

	## sets the value to arg. onchange-actions and query-to-option
	## conversions are handled here.
	def set_to(arg)
		reset
		if option? and
				(arg.is_a? String and
				(@info2.nil? or
				String === @info2 and @info2 !~ /^str/))
			arg = Convert.query_to_option(arg, @info2)
		end
		if @onchg and not @onchg.empty?
			code = @onchg
			if code =~ /^alias (\w+)$/
				## TODO: @onchg aliases
			end

			@oldvalue = @value
			@value = arg

			result = nil
			begin
				result = eval(code)
			rescue Exception
				Console.raise
			end

			@value = result unless result.nil?
		else
			@value = arg
		end
		@sortstring = @value

		Cache << self
	end

	## updates the value in the database.
	def update_value_in_database()
		if option?
			query_value = Convert.option_to_query(@value, @info2)
		else
			query_value = @value
		end

		Query.set(self, 'value', query_value)
	end

	## use set_to(arg) and if the value has changed, update it in the database.
	def value=(arg)
		oldval = @value

		## don't use destructive operations on @value, pretty please.
		## if its rly necessary, use oldval = @value.dup above
		set_to(arg)

		if oldval != @value
			update_value_in_database()
		end
	end

	## TODO: loop detection :(
	def advance(int)
		log "I (#{@value[0..20]}..)ADVANCE BY #{int}"
		self.info1 += int
		@time = Time.at(@info1)
		self.tags += ['r'] unless @tags.include?('r')
	end

	def descend(int)
		log "I (#{@value[0..20]}..)DESCEND BY #{int}"
		self.info1 -= int
		@time = Time.at(@info1)
#		self.tags += ['r'] unless @tags.include?('r')
	end

	def run_code_in_environment(code)
		env = Environment.new
		env.option = key
		env.value = val
		env.types = types
		env.oldvalue = oldval

		result = env.run(code)
		val = result unless result.nil?
	end

	def is_text?() @type == "txt" end

	def key?() not (@key.nil? or @key.empty?) end
	def value?() not (@value.nil? or @value.empty?) end
	def option?() @type == "opt" end
	def keymap?() @type == "map" end
	def command?() @type == "cmd" end
	def text?() @type == "txt" end
	def tag?() @type == "tag" end
	def time?() @time != false end
	def descr?() @descr and !@descr.empty? end
	def this() self end
	def time=(x)
		if is_text?
			self.info1 = x
			update_reoccur_option if @tags.include? 'r'
		end
		return x
	end
	def lines()
		return 1 unless text? or command?
		self.line(0) unless @line
		@line.size
	end
	def tabcomp?()
		(command?) ? (@info1 and not @info1.empty?) : (false)
	end
	def tabcomp()
		(command? and @info1) ? (@info1) : ('')
	end

	def update()
		initialize(Query.get_where("rowid=#{@rowid}"))
		reset
	end

	def reoccur!()
		return unless text?
		self.tags -= ['r']
		begin
			eval(@info2)
		rescue Exception
			lograise
		end
	end

	def to_s(wid = nil)
		wid ||= API.cols
		if @string.nil?
			generate_new_string
			@width = -1 ## to force a resize
		end
		if wid != @width
			resize(wid)
			@line = nil
		end
		return @string
	end

	def reset()
		@string = nil
		Info.request_draw = true
	end

	def delete()
		Query.delete(self)
	end

	## }}}
	## create string {{{

	def line(n)
		return "..." unless text? or command?

		return @line[n] || "<nil>" if @line

		@line = []
		if descr?
			@line << @descr
		end
		rest = @value.dup.strip
		cols = CLI.cols
		max = cols - PAD_WITHOUT_TIME

		until rest.empty?
			if rest[0] == ?\n
				rest.slice!(0)
				@line << "" unless rest.empty?
				next
			end

			current = rest[0...max]
			if ix = current.index("\n")
				current = current[0...ix]
				rest.slice!(0, ix + 1)
			else
				rest.slice!(0, max)
			end
			@line << current.ljust(max)
		end
	end

	private
	def generate_new_string
		@right = ""
		@left = ""

		txt = @type == "txt"

		## add tags
		if Array === @tags
			tmp = @tags.dup
			if Array === (hide = Opt.hide_tags_dynamic) and !hide.empty?
				tmp -= hide
			end
			if Array === (hide = Opt.hide_tags_static) and !hide.empty?
				tmp -= hide
			end
			@tagstring = tmp.join(" ")
			@right << @tagstring
		end

		## priority
		if Numeric === @prio and @prio != 0
			if @prio < -10
				str = " -#{@prio.abs}"
			elsif @prio < 0
				str = " #{'-' * @prio.to_i.abs}"
			elsif @prio > 10
				str = " +#{@prio}"
			elsif @prio > 0
				str = " #{'+' * @prio.to_i}"
			end
			@right << str
			@bold_on_the_right = str.size
		else
			@bold_on_the_right = 0
		end

		if txt
			if @info1.nil?
				@left << "       "
				@left << (@value.include?("\n") ? "**" : "  ")
				@left << "*  "
				@bold_on_the_left = PAD_WITHOUT_TIME
			else
				@left << "#{Convert.time_to_string(@info1).rjust(18)}: "
				@bold_on_the_left = PAD_WITH_TIME
			end
		else
			@bold_on_the_left = PAD_WITH_TYPE
			@left << "#{TYPES_MAP[@type].rjust(10) rescue ''}: "
		end

		if @descr and !@descr.empty? and @type != "txt"
			unless @key.nil? or (@key.is_a? String and @key.empty?)
#				@left.slice! -2
				@left << "#@key "
				@bold_on_the_left = @left.size
			end
			unless @value.nil?
				@left << "#{@descr.nolinebreaks}"
			end
		else
			unless @key.nil? or (String === @key and @key.empty?)
				@left << "#{@key}: "
			end

			unless @value.nil?
				val = (@value.is_a? String) ? @value.to_s : @value.inspect
				if pos = val.index("\n")
					if @type == 'txt'
						val = val[0, pos]
					else
						val = val.nolinebreaks
					end
				end
				@left << val
			end
		end
	end

	private
	## don't regenerate the whole string if the window is resized.
	## There are two seperate parts, a left and a right part of the string,
	## and this method puts them together.
	def resize(new_width)
		@width = new_width
		
		if @width < @right.size + 30
			## favour left side, so you can see shit when the terminal is small
			left  = @left [0..@width]
			right = @right[0..@width-left.size]
			@string = left.ljust(@width - right.size) + right
		else
			## favour right side
			right = @right[0..@width]
			left  = @left [0...@width-right.size]
			@string = left + right.rjust(@width - left.size)
		end
	end
	## }}}
end

