class Completer
	include Enumerable

	LOOP_LIMIT = 1000

	def initialize(word, array)
		word = word.to_s
		size = word.size

		@enum = proc do |&block|
			block.call(word)

			for try in array
				try = try.to_s
				block.call(try) if try[0, size] == word
			end
		end
	end

	def each(&block)
		return unless block_given?
		@enum.call do |value| yield value end
	end

	def size
		sz = 0
		each do
			sz += 1
			break if sz == LOOP_LIMIT
		end
		return sz
	end

	def [](x)
		each_with_index do |val, i|
			break if i == LOOP_LIMIT
			return val if i == x
		end
		return nil
	end
end
