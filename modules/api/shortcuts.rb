module API
	extend self

	## the alert function
	def alert
		system("amixer -q set PCM 100% unmute")
	end

	## Set or remove the sign at the bottom right that work is in progress.
	def busy(bool=true)
		UI.busy(bool)
	end

	## returns the number of lines of the screen
	def lines() CLI.lines end

	## returns the number of columns of the screen
	def cols() CLI.cols end

	## Always returns nil.
	## You might want to use this to indicate that there will be no change made, at the end of the "onchange"-code of a neuron.
	def no_change() nil end

	def exe(string)
		Command.run(string)
	end
	alias call exe

	def tabcomplete(enum)
		assert self, Environment

		if Enumerable === enum
			return Completer.new(self.words[self.word_number], enum)
		else
			return [enum, self.words.last]
		end
	end
	alias tab tabcomplete

	## run this from inside a command only. replaces abbreviation nyurons
	def abbreviate(cmd)
		assert self, Environment

		Command.run("#{cmd} #{self.rest}")
	end

	def transaction(&block)
		SQL.transaction do yield end
	end
end

