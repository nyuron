module API
	extend self

	## returns a list of Neurons. If any neurons are marked, an array
	## of them is returned. If not, it will be an array containing only
	## the neuron which is currently selected. If there are no neurons
	## on the screen at all, an array with the EmptyNeuron will be returned.
	def them()
		if UI.list.empty?
			[EmptyNeuron]
		elsif UI.marked.empty?
			[UI.list[UI.cursor]]
		else
			UI.marked.dup
		end
	end

	def mark(what = nil)
		what ||= this
		if what.is_a? Numeric
			what = UI.list[what.to_i]
		end
		return if what.is_a? EmptyNeuron
		return if what.nil?
		return if UI.marked.include? what

		UI.marked << what
	end

	def unmark(what = nil)
		what ||= this
		if what.is_a? Numeric
			what = UI.list[what.to_i]
		end
		return if what.is_a? EmptyNeuron
		return if what.nil?

		UI.marked.delete what
	end

	def mark_none
		UI.marked.clear
	end

	def mark_all
		UI.marked.replace UI.list
	end

	def mark_reverse(what=nil)
		what ||= this
		if UI.marked.include? what
			UI.marked.delete what
		else
			UI.marked << what
		end
	end

	def mark_reverse_all
		UI.marked.replace(UI.list - UI.marked)
	end
end

