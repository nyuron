module API
	extend self

	## syncs the type. look at the source for all possible keywords
	def sync(type)
		case type
		when "cmd"
			sync_commands
		when "map"
			sync_keymaps
		when "nyu"
			sync_nyurons
		when "opt"
			sync_options
		when "tag"
			sync_tags
		end
	end

	## Reload commands from the database
	def sync_commands
		busy
		Query.get_key_val_of_type("cmd") do |key, val|
			Cache.cmd[key] = val
		end
		Info.request_draw = true
	end

	## Reload tags from the database and assign correct values
	def sync_tags
		taglist = []
		for tagstring in Query.get_all_tagged_rows
			for tag in Convert.query_to_tags(tagstring.first)
				taglist << tag
			end
		end
		for key, val in Query.get_key_val_of_type('tag')
			taglist << key.strip
		end
		unique_taglist = taglist.uniq
		SQL.transaction do
			for tag in unique_taglist
				busy
				Query.manage_tag(tag)
#				Query.manage_tag(tag, taglist.count(tag))
			end
		end
		Info.request_draw = true
	end

	## Reload and parse options from the database
	def sync_options
		busy
		Query.get_key_val_of_type("opt") do |key, val|
			Opt[key] = val
		end
		Info.request_draw = true
	end

	## Reload keymaps from the database and generate
	## a new regexp that matches key-combinations
	def sync_keymaps
		busy
		combos = []
		ary = []

		## TODO: handle special keys (eg <C-R>) correctly
		Query.get_key_val_of_type("map") do |key, val|
			Cache.map[key] = val
			combos << key[0..-2] if key.size > 1
		end
		combos.uniq!

		for token in combos
			ary << token.each_char.map {|t|
				if t == '?'
					t = '\?'
				end

				"(?:#{t}"
			}.join +
				(')?' * (token.size - 1)) + ')'
		end
		UI.regexp = Regexp.new('^(?:' + ary.uniq.join('|') + ')$')
		Info.request_draw = true
	end

	## Reload all neurons from the database which match the filter
	def sync_nyurons
		busy
		filter_sql = Convert.filter_to_sql(Opt.filter)
		tags = Convert.filter_to_tags(Opt.filter)
		unless Opt.hide_tags_dynamic
			Query.create_opt("hide_tags_dynamic", tags, "arr,str")
		else
			Opt.hide_tags_dynamic = tags
		end
		UI.list.replace Query.get_neurons(filter_sql)
		Info.request_draw = true
	end

	## Request a resync, filtering and sorting of the list of neurons,
	## just before the next redraw.
	## Using refilter rather than refilter! is more efficient because even if you call it
	## multiple times it will refilter just once.
	def refilter()
		Info.request_filter = true
	end
	## Request a resorting of the list of neurons just before the next redraw.
	## Using resort rather than resort! is more efficient because even if you call it
	## multiple times it will resort just once.
	def resort()
		Info.request_sort = true
	end
	## Request a complete redraw of the whole screen, not just the changed portions.
	def redraw()
		Info.request_draw = true
	end
	
	## Resync, filter and sort the list of nyurons without a delay.
	## Use API.refilter() if possible.
	def refilter!
		sync_nyurons
		validate_cursor(false)
		UI.marked.clear
		Info.request_draw = true
		Info.request_sort = true
		Info.request_filter = false
	end

	## Resort the list of nyurons without a delay.
	## Use API.resort() if possible.
	def resort!
		busy
		bm "sorting" do
			str = Convert.sort_sequence_to_code(Opt.sort)
			begin
				Neuron.class_eval(str)
			rescue SyntaxError
				lograise
				log str
			end
			UI.list.sort!
		end
		if @@find_later
			find(@@find_later)
			@@find_later = nil
		end
		validate_cursor(false)
		Info.request_draw = true
		Info.request_sort = false
	end

	def reoccur!
		log "LET'SU REOCURRU"
		now = Time.now.to_i
		rows = Query.all_reoccuring_notes
		for row in rows
			nyu = Neuron.new(row)
			if now > nyu.info1.to_i
				nyu.reoccur!
			end
		end
		Opt.next_reoccur = 0
		Info.request_reoccur = false
		Info.request_filter = true
	end
end
