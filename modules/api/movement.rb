module API
	extend API

	@@find_later = nil

	## Make sure the cursor is an integer and not out of bounds.
	def validate_cursor(wrapping = false)
		## The cursor has to be an integer. :)
		case UI.cursor
		when Float
			UI.cursor = UI.cursor.round
		when Integer
		else
			UI.cursor = UI.cursor.to_i
		end

		## Modification of the cursor -------------
		## there are no items to show
		if UI.list.size == 0
			UI.cursor = 0

		## cursor is above the screen
		elsif UI.cursor < 0
			if wrapping
				UI.cursor += UI.list.size
				UI.cursor = 0 if UI.cursor < 0
			else
				UI.cursor = 0
			end

		## cursor is below the screen
		elsif UI.cursor >= UI.list.size
			UI.cursor = UI.list.size - 1
		end

		## Modification of the start position -----
		## screen is big enough to show all
		if UI.list.size <= lines - 1
			UI.start = 0

		## list size shrinks and screen is too far down
		elsif UI.start + lines - 2 >= UI.list.size
			UI.start = UI.list.size - lines

		## cursor gets below the screen
		elsif UI.start + lines - 2 <= UI.cursor
			UI.start = UI.cursor - lines + 2

		## cursor gets above the screen
		elsif UI.cursor < UI.start
			UI.start = UI.cursor
		end
	end

	## Move by n down in the list of neurons
	def move(n)
		UI.cursor += n
		validate_cursor(false)
	end

	## Go to the position n in the list of neurons.
	def goto(n)
		UI.cursor = n
		validate_cursor(true)
	end

	## find a neuron object in the list and go to it. The searching is done
	## by comparing the row_id. this function accepts either a rowid
	## (as a number or string) or a neuron object.
	def find(row_id)
		unless row_id.is_a? Numeric
			if Neuron === row_id
				row_id = row_id.row_id
			elsif String === row_id
				row_id 
			else
				return
			end
		end

		found = UI.list.index_with_block {|x| x.row_id == row_id}
		if found
			UI.cursor = found
			validate_cursor
		end
	end

	## calls find *after* the next sort is completed.
	def find_later(x)
		@@find_later = x
	end

	## search a string in the filtered list and go to its position.
	## the first argument may be a regexp, a string which will be converted
	## into a regexp, or a symbol: :next or :prev.
	def scan(rx, step = 1)
		pos = UI.cursor
		lsize = UI.list.size
		if lsize <= 1
			Console.write "list too small to seach!"
		end

		if rx.is_a? Symbol
			step *= -1 if rx == :prev
			lastmode = Console.last_search_mode
			return unless lastmode
			if rx.in :next, :prev
				rx = Console.get_history[lastmode].first
			end
			step *= -1 if lastmode == :searchback
		end
		if rx.is_a? String
			rx = Regexp.new(rx, true)
		end

		assert rx, Regexp
		
		lsize.times do
			pos += step
			if pos >= lsize
				pos = 0
				Console.write "wrap!"
			elsif pos < 0
				pos = lsize - 1
				Console.write "wrap!"
			end

			if nyu = UI.list[pos]
				if (key = nyu.key).is_a? String
					return goto pos if key =~ rx
				end
				if (val = nyu.value).is_a? String
					return goto pos if val =~ rx
				end
			end
		end
		Console.write "not found!"
	end
end

