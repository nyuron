module API
	extend self

	## returns the Neuron object which is currently pointed at
	## or the EmptyNeuron if there are no neurons in the list.
	def this()
		if UI.list.empty?
			EmptyNeuron
		else
			UI.list[UI.cursor]
		end
	end

	VALID_EDIT_KEYWORDS = %w(prio tags key value descr onchg ctime mtime info1 info2 info3)

	## edit an attribute of the targetted neuron in an external editor.
	## the parameter *what* must be an element of VALID_EDIT_KEYWORDS.
	def edit(what)
		what = what.to_s
		return unless VALID_EDIT_KEYWORDS.include? what
		current_content = Query.get(this, what)

		filename = "/tmp/nyuron.#{Process.pid}.#{rand 1000000}"
		file = File.new(filename, 'w')
		file.write current_content
		file.close

		CLI.closei
		system(Info.editor % filename)
		CLI.starti

		new_content = File.read(filename)
		new_content.slice!(-1) if new_content[-1] == ?\n ##quick&dirty fix
		File.delete(filename)

		this.send("#{what}=", new_content)

		if what == 'key'
			sync(this.type)
		end
	end

	VALID_NEW_KEYWORDS = %w(cmd map opt tag txt)

	## create a new entry.
	## the parameter *what* must be an element of VALID_NEW_KEYWORDS.
	## *key* and *value* are optional
	def new(what, key=nil, value=nil)
		what = what.to_s
		return unless VALID_NEW_KEYWORDS.include? what
		return if (key and not key.empty? and Query.exists?(what, key))

		result = Query.create_and_return(what, key, value)
		refilter!
		resort!
		return result
	end

	## Wrapper for Neuron#delete(what)
	def delete(what)
		what.all.delete
	end
end
