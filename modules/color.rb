require 'ncurses'

module Color
	extend Color

	COLORSCHEMEDIR = File.join(MYDIR, 'data', 'colorscheme')
	def load_colorscheme(name)
		## colorschemes are located in data/colorscheme/
		fname = File.join(COLORSCHEMEDIR, "#{name}.rb")
		assert File.exists?(fname), "No such colorscheme: #{fname}"
		
		clear_all
		load fname
		::Console.write("Colorscheme #{name} loaded.")
	end

	def clear_all()
		clear
		for key, type in TYPES
			type.clear
		end
	end

	def clear
		for var in instance_variables
			instance_variable_set(var, nil)
		end
	end

	def default() -1 end
	def black()    0 end
	def red()      1 end
	def green()    2 end
	def yellow()   3 end
	def blue()     4 end
	def magenta()  5 end
	def cyan()     6 end
	def white()    7 end

	alias brown yellow
	alias orange yellow
	alias purlpe magenta
	alias pink magenta
	alias teal cyan
	alias gray white
	alias grey white

	def normal()     Ncurses::A_NORMAL     end
	def bold()       Ncurses::A_BOLD       end
	def reverse()    Ncurses::A_REVERSE    end
	def underline()  Ncurses::A_UNDERLINE  end

	def standout()   Ncurses::A_STANDOUT   end
	def blink()      Ncurses::A_BLINK      end
	def dim()        Ncurses::A_DIM        end
	def protect()    Ncurses::A_PROTECT    end
	def invisible()  Ncurses::A_INVIS      end
	def altcharset() Ncurses::A_ALTCHARSET end
	def chartext()   Ncurses::A_CHARTEXT   end

	alias reversed reverse
	alias revert reverse
	alias invis invisible
	alias none normal

	def default_color() return default, default, none end
	alias dc default_color

	## a shortcut.
	##    use %w{txt type left_side}
	## is equivalent to:
	##    def txt() @txt || @type || @left_side || @base end
	def self.use(arr)
		arr << 'base' unless arr.last == 'base'
		body = arr.map{|x| "@#{x}"}.join(' || ')
		eval "def #{arr.first}() #{body} end"
	end

	use %w{cmd type left_side}
	use %w{map type left_side}
	use %w{opt type left_side}
	use %w{tag type left_side}
	use %w{txt type left_side}

	use %w{date left_side}
	use %w{type left_side}

	use %w{tags}
	use %w{base}
	use %w{content}
	use %w{left_side}
	use %w{priority}
	use %w{priority_negative priority}
	use %w{priority_positive priority}
	use %w{error}
	use %w{info}

	def content!() @content end
	def terminal_cursor() @terminal_cursor end

	module Type
		include Color

		ATTRIBUTES = %w[
			base content tags
			priority_negative priority_positive priority
			cmd map tag opt txt
			date type left_side
			error info
			terminal_cursor
		]

		for a in ATTRIBUTES
			eval <<-DONE
				def #{a}()
					@#{a}_cache ||= @#{a} || super || Color.#{a} || default_color
				end
			DONE
		end

		## this is only meant to be used in Color
		def clear_all() nil end
	end

	module Normal;   extend Type end
	module Selected; extend Type end
	module Marked;   extend Type end
	module Done;     extend Type end
	module Console
		extend Type
		@terminal_cursor = true
		def clear
			super
			@terminal_cursor = true
		end
	end

	TYPES = {
		:normal => Normal,
		:selected => Selected,
		:marked => Marked,
		:done => Done,
		:console => Console
	}

	def [](x)      TYPES[x] end
	## ugly cause of name clashes
	def sel() Selected end
	def nor() Normal   end
	def mar() Marked   end
	def don() Done     end
	def console() Console end
end

