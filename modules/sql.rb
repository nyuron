## a little class for easy access to the sqlite database

require 'sqlite3'

module SQL
	extend SQL
	def self.initialize()
		@@db = SQLite3::Database.new( Info.dbfile )
		@@db.create_function('regexp', 2) do |func, pattern, expression|
			regexp = Regexp.new(pattern.to_s, Regexp::IGNORECASE)
			if expression.to_s.match(regexp)
				func.result = 1
			else
				func.result = 0
			end
		end
	end

	## some wrapper methods
	def sql_begin( &block )
		@@db.transaction( &block )
	end

	def self.transaction( &block )
		@@db.transaction( &block )
	end

	def sql_execute( *query, &block )
		log "sql: #{query.join("; ")}"
		@@db.execute( *query, &block )
	end

	def sql_first( *query, &block )
		@@db.get_first_row( *query, &block )
	end

	def sql_end()
		@@db.commit
	end

	def sql_prepare( query )
		@@db.prepare( query )
	end

	def sql_last_row()
		@@db.last_insert_row_id()
	end

	## a quick bugfix. multiple statements in a string
	## don't seem to work right now, so i split them up.
	def sql_each_line( query )
		query.each_line do |line|
			sql_execute( line )
		end
	end

	## a comfortable alias
	def sql( *query, &block )
		if query.empty? then
			sql_begin( &block )
		else
			sql_execute( *query, &block )
		end
	end
end

