class Void; end

module Enumerable
	## a shortcut for map {...}
	##  [10, 20, 30].all + 5           => [15, 25, 35]
	##  [10, 20, 30].map { |x| x + 5 } => [15, 25, 35]
	##  %w(hello you).all.upcase       => ["HELLO", "YOU"]

	def all
		Mapper.new(self)
	end

	## This class is used in Enumerable#all
	class Mapper < Void
		def initialize(obj)
			@obj = obj
		end
		def method_missing(*args)
			@obj.map do |item|
				item.send(*args)
			end
		end
	end
end

