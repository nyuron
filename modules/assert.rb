module Debug
	class AssertionError < StandardError; end

	## Passes if value is neither nil nor false.
	## The second argument is an optional message.
	## All other assert methods are based on this.
	def assert_true(value, message = nil)
		message ||= "#{value.inspect} is not true!"
		Kernel.raise(AssertionError, message, caller(0)) unless value
	end

	## Takes a good guess about what you want to do.
	## There are two options:
	## * 1 or 2 arguments, of which the second must be a String
	##
	##   use assert_true(value, rest.first)
	## * otherwise
	##
	##   use assert_match(value, *rest)
	def assert(value, *rest)
		if rest.size == 0 or rest.size == 1 and rest.first.is_a? String
			assert_true(value, rest.first)
		else
			assert_match(value, *rest)
		end
	end

	## Passes if "testX === value" is true for any argument.
	## If the last argument is a string, it will be used as the message.
	def assert_match(value, test0, *test)
		## test0 and *test are only seperated to force >=2 args
		## so put them back together here.
		test.unshift( test0 )

		## get or generate the message
		if test.last.is_a? String
			message = test.pop
		else
			message = "Expected #{value.inspect} to match with "
			message << if test.size == 1
				"#{test0.inspect}!"
			else
				"either #{test.map{|x| x.inspect}.join(" or ")}!"
			end
		end

		assert_true test.any? { |testX| testX === value }, message
	end

	## Passes if "value1 == value2"
	def assert_equal(value1, value2, message=nil)
		message ||= "#{value1.inspect} expected, got: #{value2.inspect}!"
		assert_true value1 == value2, message
	end

	## Passes if "value1 != value2"
	def assert_not_equal(arg1, arg2, message=nil)
		message ||= "Expected something other than #{arg1.inspect}!"
		assert_true arg1 != arg2, message
	end

	## Put this at positions which require attention.
	def flunk(message = nil)
		message ||= "Incomplete Code!"
		assert_true false, message
	end

	alias forbid flunk
	alias assert_neq assert_not_equal
end

