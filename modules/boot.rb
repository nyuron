require 'pathname'

module Boot
	extend self
	ROOT_DIRECTORY = '/'

	def get_conf_path(*locations)
		locations += [
			File.join(ENV['HOME'], '.nyurc'),
			File.join(ROOT_DIRECTORY, 'etc', 'nyuron.conf'),
			File.join(MYDIR, 'nyuron.conf')
		]

		find_first_existing_file(*locations)
	end

	def get_database_path(*locations)
		get_database_path_and_places(*locations).first
	end

	def get_database_path_and_places(*locations)
		locations += [
			File.expand_path('~/.nyudb'),
			File.join(MYDIR, 'db')
		]

		return find_first_existing_file(*locations), locations
	end

	def find_first_existing_file(*locations)
		for path in locations 
			next unless path.is_a? String
			next unless (pn = Pathname.new(path)).exist?
			path = pn.realpath.to_s
			return path if File.exists?(path)
		end
		return nil
	end
end
