require 'modules/cli'
module UserInterface
	extend CLI
	extend self

	attr_accessor :list, :regexp
	attr_reader :cursor, :start, :marked

	@keybuffer = ""
	@regexp = //
	@cursor = 0
	@busy_array = %w(/ - \\ |)
#	@busy_array = ("!".."~").to_a.shuffle
	@busy_pos = 0
	@start = 0
	@old_start = 0
	@old_cursor = 0

	@marked = []
	@list = []

	def meta_loop
		Cache.opt.sync!
		Cache.map.sync!
		Cache.cmd.sync!

		while true
			begin
				$now = Time.now
				do_tasks
				case Info.where
				when :main
					CLI.cursor(Color.terminal_cursor)
					main_loop
				when :console, :search, :searchback
					CLI.cursor(Color.console.terminal_cursor)
					Console.main_loop(Info.where)
				else
					forbid
				end
			rescue
				Console.raise $!
			end
		end
	end

	def main_loop
		API.busy true
		UI.draw
		Console.clear
		API.busy false
		CLI.movi UI.cursor - UI.start, 0
		key = UI.get_key
		API.busy true
		UI.handle_key(key)
	end

	def do_tasks
		if Info.request_filter
			API.refilter!
		end
		if Info.request_reoccur or
				((next_reoccur = Opt.next_reoccur.to_i) and 
				next_reoccur > 0 and
				$now.to_i > next_reoccur)
			API.reoccur!
		end
		if Info.request_sort
			API.resort!
		end
	end

	def key_combination?(str)
#		log("#{str} =~ #{@regexp} => #{str =~ @regexp}")
		str =~ @regexp
	end

	## Returns the line, where the console is drawn.
	def console_line()
		-1
	end
	
	## Get the key from user input
	def get_key()
		geti
	end

	def handle_key(key)
		@key = key
		return if @key.empty?

		# special cases
		case @key
		when '<'
			@key = "<<"
		when '<redraw>'
			Info.request_draw = true
			return
		end

		@keybuffer << @key

		if key_combination?(@keybuffer).nil?
			command = Cache.map[@keybuffer]
			if command
				Command.run(command, @keybuffer)
			end
			@keybuffer.clear
		end
	end

	## Set or remove the sign at the bottom right that work is in progress.
	## Also, each time this method is called with true, that sign is rotated.
	## Use this in time-expensive processes to entertain the user :)
	def busy(bool=true)
		return unless Opt.busy
		if bool
			@busy_pos += 1
			if @busy_pos == @busy_array.size
				@busy_pos = 0
			end
			puti(-1, -1, @busy_array[@busy_pos])
			CLI.refresh
		else
#			if Info.where == :main
#				puti(-1, -1, "]")
#			else
				puti(-1, -1, " ")
#			end
			CLI.refresh
		end
	end

	def draw
#		bm "draw" do
			width = API.cols
			cursor_color = Opt.cursor_color
			cursor_color = [6, 0] unless cursor_color.is_a?(Array) and cursor_color[0].is_a?(Integer) and cursor_color[1].is_a?(Integer)
			
			Info.request_draw = true ##temporary
			if Info.request_draw or @old_start != @start or (@cursor != @old_cursor and !@list[@old_cursor])
				Info.request_draw = false

				expand = Opt.expand
				expand = false unless expand == false or expand == true
				expand_max = Opt.expand_max
				expand_max = 10 unless expand_max.is_a? Integer
				expand = false if expand_max < 1

				draw_blank = false

				line_ix = 0
				list_ix = 0
				while line_ix + 1 < lines
					jump = 1
					if draw_blank
						attr_set(*Color[:normal].base)
						puti(line_ix, " " * cols)

					else
						cell = @list[list_ix+@start]
						if cell.nil?
							draw_blank = true
							redo
						end

						if @cursor - @start == line_ix
							mode = :selected
						elsif @marked.include? cell
							mode = :marked
						elsif cell.tags.include? 'k'
							mode = :done
						else
							mode = :normal
						end

						attr_set(*Color[mode].content)
						puti(line_ix, cell.to_s(width))
						attr_set(*Color[:normal].content)
#						log "cell.lines = #{cell.lines}"
						if expand and mode == :selected and cell.lines > 1
#							log("expand and cell.lines > 1")
							for line in 1..[cell.lines - 1, expand_max].min
								puti(line_ix+jump, cell.line(line).rjust(cols))
#								attr_at(0, line_ix+jump, -1, *Color[:selected].content)
								attr_at(0, line_ix+jump, -1, *Color[:normal].content)
								jump += 1
							end
							if (cell.lines > expand_max + 1)
								stringy = "...."
							else
								stringy = ""
							end
							puti(line_ix+jump, stringy.ljust(cols))
							attr_at(0, line_ix+jump, -1, *Color[:normal].content)
							jump += 1
						end
#						if mode == :selected
#							attr_set(-1, -1, 0)
#						else
						if true
							if cell.bold_on_the_left > 0
#								logpp Color[mode].map
								clr = Color[mode].send(cell.type) #rescue Color.default_color
								attr_at(0, line_ix, cell.bold_on_the_left, *clr)
							end

							botr = cell.bold_on_the_right
							if botr > 0
								if cell.prio < 0
									clr = Color[mode].priority_negative
								else
									clr = Color[mode].priority_positive
								end
								attr_at(cols-botr, line_ix, botr, *clr)
							end

							if not cell.tags.empty?
#								botr += 1 if botr > 0
								sz = cell.tagstring.size
								clr = Color[mode].tags
								attr_at(cols-botr-sz, line_ix, sz, *clr)
							end
						end
						attr_set(-1, -1, 0)
					end
					
					list_ix += 1
					line_ix += jump
				end
			else
				if @cursor != @old_cursor
					old = @old_cursor - @start
					cur = @cursor - @start
					attr_at(0, cur, -1, *Color[:selected].content)
#					color_reverse_at(cur, 0, -1, *cursor_color)
					attr_at(0, old, -1, *Color[:normal].content)
#					color_at(old, 0, -1, -1, -1)
					cell = @list[@old_cursor]
#					color_bold_at(old, 0, cell.bold_on_the_left, -1, -1) if cell.bold_on_the_left > 0
#					color_at(old, cols-cell.bold_on_the_right, -1, 2, -1) if cell.bold_on_the_right > 0
##					color_bold_at(old, cols-cell.bold_on_the_right, -1, -1, -1) if cell.bold_on_the_right > 0
##						color_at(cur, 0, 13, 4, 6)
					color -1, -1
				end
#			end

			@old_start = @start
			@old_cursor = @cursor
		end
	end

	def start=(val)
		if @start != val
			@start, @old_start = val, @start
		end
	end

	def cursor=(val)
		if @cursor != val
			@cursor, @old_cursor = val, @cursor
		end
	end

	def close
		closei
		puts Opt.quitmsg
	end
end

UI = UserInterface
