## modules/query
require 'modules/sql'

## all the sql queries are supposed to be here
## TODO: THIS NEEDS A LOT WORK
module Query
	include SQL
	extend Query

	COLUMNS = %w(prio tags key value descr onchg ctime mtime info1 info2 info3)
	TYPES = %w(cmd map opt tag txt)

	CREATE = "INSERT INTO nyu (type, key, value) VALUES (?, ?, ?)"
	CREATE_OPT = "INSERT INTO nyu (type, key, value, info2) VALUES ('opt', ?, ?, ?)"
	DELETE_ALL_CONFIG_DATA = "DELETE FROM nyu WHERE NOT (type='txt' or type='tag')"
	SET = {}
	TIME_LEFT = "SELECT info1 FROM nyu WHERE type='txt' AND (NOT info1='') AND info1>? AND tags GLOB '*/i/*' ORDER BY info1 LIMIT 1;"
	GET_ALL_CONFIG_DATA = "SELECT * FROM nyu WHERE NOT (type='txt' OR type='tag')"
	INSERT_WITHOUT_ROWID = "INSERT INTO nyu (key,type,ctime,mtime,prio,tags,value,descr,onchg,info1,info2,info3) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?)"
	UPDATE_CONFIG_DATA = "UPDATE nyu SET key=?, type=?, ctime=?, mtime=?, prio=?, tags=?, value=?, descr=?, onchg=?, info1=?, info2=?, info3=? WHERE type=? AND key=?"

	TABLE_CREATION = <<SQL
CREATE TABLE nyu(
	rowid INTEGER PRIMARY KEY ASC,
	key TEXT,
	type TEXT,
	ctime INTEGER,
	mtime INTEGER,
	prio REAL,
	tags TEXT,
	value TEXT,
	descr TEXT,
	onchg TEXT,
	info1 TEXT,
	info2 TEXT,
	info3 TEXT
);
SQL


	INSERT = "INSERT INTO nyu"
	INSERT2 = "#{INSERT} (key, type, value) VALUES"

	SELECT = "SELECT * FROM nyu"
	SELECT_VALUE = "SELECT value FROM nyu"

	GET_NYURONS = "#{SELECT}"

	for column in COLUMNS
		SET[column] = "UPDATE nyu SET #{column}=? WHERE rowid=?"
	end

	def create_table()
		sql do
			sql TABLE_CREATION
			sql_each_line DEFAULTS
		end
	end

	def get_time_left()
		x = sql_first(TIME_LEFT, Time.now.to_i)
#		x = @@time_left.execute(Time.now.to_i).first
		if x
			return x.first.to_i
		else
			return nil
		end
	end

	def export_options( fname )
		fname = File.expand_path(fname)
		assert_neq fname, File.expand_path(Info.dbfile),
			"Cannot export to current database!"

		db = SQLite3::Database.new( fname )
		db.execute TABLE_CREATION
		db.transaction
		sql GET_ALL_CONFIG_DATA do |row|
			row.shift
			db.execute(INSERT_WITHOUT_ROWID, *row)
		end
		db.commit
		db.close
	end

	def import_options( fname, loop_detected = false )
		fname = File.expand_path(fname)
		assert_neq fname, File.expand_path(Info.dbfile),
			"Cannot import from current database!"

		bk = "/tmp/nyu_backup_#{rand 100000}"
		export_options( bk )

		db = SQLite3::Database.new( fname )
		sql_begin
		sql DELETE_ALL_CONFIG_DATA
		db.execute GET_ALL_CONFIG_DATA do |row|
			row.shift
			sql(INSERT_WITHOUT_ROWID, *row)
		end

		begin
			sql_end
		rescue
			Console.error "import failed"
			unless loop_detected
				import_options(bk, true)
			end
		ensure
			db.close
			File.delete(bk)
		end
	end

	def merge_options( fname )
		fname = File.expand_path(fname)
		assert_neq fname, File.expand_path(Info.dbfile),
			"Cannot merge from current database!"

		db = nil
		sql_begin
		begin
			db = SQLite3::Database.new( fname )
			db.execute GET_ALL_CONFIG_DATA do |row|
				row.shift
				othertype = row[1]
				otherkey  = row[0]
				if mtime = Query.get_mtime_from_type_key(othertype, otherkey)
					mtime = mtime.first
					othertime = row[3]

					if mtime.nil? or (othertime and mtime < othertime)
						sql UPDATE_CONFIG_DATA, *(row + [othertype, otherkey])
					end
				else
					sql INSERT_WITHOUT_ROWID, *row
				end
			end
		ensure
			db.close if db
			sql_end
		end
	end

	def exists?(type, key=nil)
		sql_first("SELECT value FROM nyu WHERE type=? AND key=?", type, key)
	end

	def get_neurons(filter)
		list = []
		addnyurons = proc do |row|
			list << Neuron.new(row)
		end

		query = GET_NYURONS
		query += " WHERE #{filter}" unless filter.strip.empty?
		begin
			sql(query, &addnyurons)
		rescue SQLite3::SQLException
			begin
				list = []
				sql(query, &addnyurons)
			rescue Exception
				Console.error
			end
		rescue Exception
			Console.error
		end
		return list
	end


	def set(rowid, key, val)
		if Neuron === rowid
#			nyuron = rowid
			rowid = rowid.rowid
#			option = nyuron.option? if key == 'value'
		elsif SQLite3::ResultSet::ArrayWithTypesAndFields === rowid
			rowid = rowid.first.to_i
		end

		assert rowid, Numeric

#		if option
#			set_option(nyuron.key, val) ## this will also eval the code of some options
#		else
#			sql("UPDATE nyu SET #{key}=\"#{val.sql}\" WHERE rowid=#{rowid}")
			if SET.has_key?(key)
				sql SET[key], val, rowid
#				@@set[key].execute(val, rowid)
			else
				forbid
			end
#			sql("UPDATE nyu SET #{key}=? WHERE rowid=?", val, rowid)
#			trace
#		end
	end

	def create(type, key=nil, val=nil)
		sql CREATE, type, key, val
#		@@create.execute(type, key, val)
		neuron = Neuron.from_rowid(sql_last_row)
		Cache << neuron
		return neuron
	end
	alias create_and_return create

	def create_opt(key=nil, val=nil, info2=nil)
		sql CREATE_OPT, key, nil, info2
		opt = Neuron.from_rowid(sql_last_row)
		opt.value = val
		Cache << opt
		return opt
	end
	alias create_opt_and_return create_opt

	def get(rowid, key)
		rowid = rowid.rowid if Neuron === rowid
		assert rowid, Numeric

		sql_first("SELECT #{key} FROM nyu WHERE rowid=#{rowid}")
	end

	def get_mtime_from_type_key(type, key)
		sql_first("SELECT mtime FROM nyu WHERE type=? AND key=?", type, key)
	end

	def get_all_where(str)
		sql("SELECT * FROM nyu WHERE " + str)
	end

	def get_all_tagged_rows()
		sql("SELECT tags FROM nyu WHERE tags IS NOT NULL")
	end


	def get_all_tags()
		sql("SELECT key FROM nyu WHERE type='tag'").map!{|x| x.first}
	end

	def get_key_val_of_type(type, &block)
		sql("SELECT key, value FROM nyu WHERE type=?", type, &block)
	end

	def get_where(str, *args)
		sql_first("SELECT * FROM nyu WHERE " + str, *args)
	end

	def get_mapping(keybuffer)
		result = sql("#{SELECT_VALUE} WHERE type='map' AND key='#{keybuffer}' LIMIT 1").first
		return result ? result.first : nil
	end
	
	def get_command(command)
		result = sql("#{SELECT_VALUE} WHERE type='cmd' AND key='#{command}' LIMIT 1").first
		return result ? result.first : nil
	end

	def get_tabcomp(cmd)
		sql_first("SELECT info1 FROM nyu WHERE type='cmd' AND key='#{cmd}'")
	end

	def get_option_info(key)
		sql_first("SELECT value, info2, info3 FROM nyu WHERE type='opt' AND key='#{key}'")
	end

	def all_reoccuring_notes(&block)
		sql("SELECT * FROM nyu WHERE type='txt' AND NOT info2='' AND tags GLOB '*/r/*'", &block)
	end

	def db_size()
		sql_first("SELECT count(*) FROM nyu").first.to_i
	end

	def delete(what)
		if Neuron === what
			Cache.delete(what)
			what = what.rowid
		end
		assert what, Numeric
		sql("DELETE FROM nyu WHERE rowid=#{what}")
	end

	def set_option(key, val)
		result = Query.get_option_info(key)
		oldval, types, code = result

		catch :break do
			if code
				if code.strip =~ /^alias (\w+)$/
					code = Query.get_option_code($1)
					throw :break unless String === code
				end
				env = Environment.new
				env.option = key
				env.value = val
				env.types = types
				env.oldvalue = oldval

				result = env.run(code)
				val = result unless result.nil?
			end
		end

		Cache.opt[key.to_sym] = Convert.option(val,types)
		sql("UPDATE nyu SET value=? WHERE type='opt' AND key=?", val, key)
	end

	def manage_tag(name, number = nil, rowid = nil)
		return unless String === name and not name.empty?
		assert number, Integer, NilClass
		if number.nil?
			number = sql("SELECT count(*) FROM nyu WHERE tags glob '*#{Convert::TAG_SPLIT_STRING}#{name}#{Convert::TAG_SPLIT_STRING}*'")
			number ||= 0
		end
		if rowid.nil?
			rowid = sql_first("SELECT rowid FROM nyu WHERE type='tag' AND key=?", name)
		end
		if rowid
			set(rowid, "value", number)
		else
			create('tag', name, number)
			API.refilter
		end
	end
end
