module Convert
	extend Convert

	## what is the string the tags are encapsulated and seperated with?
	TAG_SPLIT_STRING = '/'

	## the range where the tags are, usually 1...-1.
	## this extracts "foo|bar" out of "|foo|bar|"
	TAG_SPLIT_RANGE = (TAG_SPLIT_STRING.size ... -TAG_SPLIT_STRING.size)

	## convert an array of Tags (strings) to the correct representation in the
	## sql database. That is
	def tags_to_query(array)
		assert array, Array
		if array.empty?
			""
		else
			## ["a", "b"] => "|a|b|" if TAG_SPLIT_STRING is "|"
			"#{TAG_SPLIT_STRING}#{array.join(TAG_SPLIT_STRING)}#{TAG_SPLIT_STRING}"
		end
	end

	## convert the sql-representation of tags in the database to a ruby object
	def query_to_tags(string)
		assert string, String, NilClass
		if string.nil? or string.empty?
			return []
		else
			string[TAG_SPLIT_RANGE].split(TAG_SPLIT_STRING)
		end
	end

	## converts a user entered string to an array of tags
	def string_to_tags(string)
		assert string, String
		result = string.split(/\s+|\s*,\s*/)
#		result.empty? ? nil : result
	end

	## converts an array of tags to a valid string
	def tags_to_string(tags)
		if Array === tags
			tags.join(' ')
		else
			''
		end
	end
end

