require 'modules/assert'
require 'classes/extensions/numeric'

include Debug

module Convert
	extend self

	## time_to_string {{{

	## Converts a Time object or timestamp to a string for output
	## on the screen using mostly Time#strftime

	def time_to_string(time)
		if Numeric === time
			time = Time.at(time)
		elsif String === time
			time = Time.at(time.to_i)
		end
		assert time, Time

		if (time - $now).between?(0, 7.days)
			return time.strftime '%A, %H:%M'
		end
		return time.strftime '%a, %e.%b %H:%M'
	end
	## }}}

	## string_to_time {{{
	
	## Converts a string, supposedly typed in by the user, to a time object or nil.
	## You may chose a default object which is returned if no pattern is found in the string.
	## nil is returned if the string is empty or if you don't specify a default object.
	##  Convert.string_to_time("now")        => Fri May 29 01:23:45 +0200 2009
	##  Convert.string_to_time("15:00")      => Fri May 29 15:00:00 +0200 2009
	##  Convert.string_to_time("20.5 23:59") => Fri May 20 23:59:00 +0200 2009

	MONTHS = %w(jan feb mar apr may jun jul aug sep oct nov dec)
	WEEKDAYS = %w(su mo tu we th fr sa)

	## $1: month (int or name), $2: mday, $3: possibly weekday, $4: year
	REGEXP_DATE = <<-'DONE'
		(?:
			( \d\d? | \w{3,9} )
			\s* /? \s*
			( \d\d? )
			(?:\.|th|st|nd|rd)?
			|
			( (?:last\ )? \w{3,9} )
		)
		(?:
			[\s/]+
			( \d{2} | \d{4} )
		)?
	DONE

	## This method parses the output from a match with REGEXP_DATE
	## and returns the year, month, day and a number which has
	## to be added to the time.
	def regexp_date_to_i(r1, r2, r3, r4)
		add = nil
		if r3
			if r3[0, 4] == 'last'
				r3.slice! 0...5
				neg = true
			else
				neg = false
			end

			word2 = r3[0,2].downcase
			word3 = r3[0,3].downcase

			if WEEKDAYS.include? word2
				if neg
					add = -WEEKDAYS.index(word2) - $now.wday
					add = -add.wrap(0..7) * 1.day
					add = -7.days if add == 0
				else
					add = WEEKDAYS.index(word2) - $now.wday
					add = add.wrap(0..7) * 1.day
					add = 7.days if add == 0
				end
			elsif word3 == 'tom'
				add = 1.day
			elsif word3 == 'tod'
				add = 0
			end

			if add
				day = $now.day
				month = $now.month
			end
		end

		unless add
			add = 0

			## month
			month = nil
			if r1
				if r1.is_word?
					month = MONTHS.index(r1[0,3].downcase)
					month += 1 if month
				elsif r1.is_integer?
					month = r1.to_i
				end
			else
				month = $now.month
			end
			day = r2.to_i
		end

		## year
		if r4.is_a? String
			year = r4.to_i
			if r4.size == 2
				year += 2000
				year -= 100 if year > 2066
			end
			unless year.between?(1966, 2066)
				year = $now.year
			end
		else
			year = $now.year
		end

		return year, month, day, add
	end

	## $1: hour, $2: minute, $3: second (or nil)
	REGEXP_TIME = <<-'DONE'
		( \d\d? ) : ( \d\d )
		(?:
			: ( \d\d )
		)?
	DONE

	## This method parses the output from a match with REGEXP_TIME
	## and returns the hour, minute and second
	def regexp_time_to_i(r1, r2, r3)
		hour   = r1.to_i
		minute = r2.to_i
		second = r3.to_i
		return hour, minute, second
	end

	def string_to_time(str, default = nil)
		assert str, String

		return nil if str.empty?

		return $now if str == 'now'

		if str =~ /^#{REGEXP_TIME}$/x
			hour, minute, second = regexp_time_to_i($1, $2, $3)
			return Time.local($now.year, $now.month, $now.day, hour, minute, second)
		end

		if str =~ /^#{REGEXP_DATE}(?:[,;\/\\]?\s*#{REGEXP_TIME})?$/x
			year, month, day, add = regexp_date_to_i($1, $2, $3, $4)
			hour, minute, second = regexp_time_to_i($5, $6, $7)

			logpp [add, year, month, day, hour, minute, second]
			if month
				t = Time.local(year, month, day, hour, minute, second) + add
				return log t
			end
		end

		Console.write("please rephrase the date, eg:  May 23th 2003, 13:37")

		return default
	end
	## }}}

	## string_to_relative_time {{{
	def string_to_relative_time(str, original, dir)
		assert str, String
		if str.empty?
			nil
		elsif str =~ /^(\d+)\s*hours?/
			Time.now + dir * $1.to_i * 3600
		elsif str =~ /^(\d+)\s*minutes?/
			Time.now + dir * $1.to_i * 60
		elsif str =~ /^(\d+)\s*(?:seconds?)?/
			Time.now + dir * $1.to_i
		else
			original
		end
	end
	## }}}

	## relative_time_to_string {{{

	def relative_time_to_string(time)
		time = time.to_i - Time.now.to_i

		if time < 0
			""
		elsif time > 72.hours
			"%dd" % [time/1.day]
		else
			"%d:%02d" % [time/1.hour, time/60 % 60]
		end
	end

	## }}}
end
