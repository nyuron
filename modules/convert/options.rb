module Convert
	extend Convert
	## Use query_to_option to convert the string value of an option
	## fresh out of the database or from user input into a ruby object
	##
	##  Convert.query_to_option("1,2,3", nil)                       => "1,2,3"
	##  Convert.query_to_option("1,2,3", "")                        => "1,2,3"
	##  Convert.query_to_option("1,2,3", "sym")                     => :"1,2,3"
	##  Convert.query_to_option("1,2,3", "arr")                     => ["1", "2", "3"]
	##  Convert.query_to_option("1,2,3", "arr,int")                 => [1, 2, 3]
	##  Convert.query_to_option("1,2,3", "arr,flt,int")             => [1.0, 2, 3]
	##  Convert.query_to_option("1,2,3", "arr,sym,str,int,flt,str") => [:"1", "2", 3]

	def query_to_option(val, types)
		return val unless String === types

		type_array = types.split(',')
		main_type = type_array.shift

		if main_type == "arr"
			last_type = nil
			val.split(',').combine!(type_array).map! do |val, type|
				last_type = type if type
				query_to_option_apply_type(val, last_type)
			end
		else
			query_to_option_apply_type(val, main_type)
		end
	end

	## This is the opposite of query_to_option. It converts ruby objects into
	## a format which is ready to be written into the database.
	##
	## Examples:
	##  Convert.option_to_query([true, false], "arr,bol") => "true,false"
	##  Convert.option_to_query([true, false], "str")     => "truefalse"
	##  Convert.option_to_query("fooo", "arr,int")        => "0"

	def option_to_query(val, types)
		return val.to_s unless String === types

		type_array = types.split(',')
		main_type = type_array.shift

		if main_type == "arr"
			last_type = nil
			val.to_a.combine(type_array).map! do |val, type|
				last_type = type if type
				option_to_query_apply_type(val, last_type)
			end.join(",")
		else
			option_to_query_apply_type(val, main_type)
		end
	end

	## This function is used by query_to_option. It does not handle arrays,
	## because arrays are taken care of in query_to_option.
	##  Convert.query_to_option_apply_type("5", "int") => 5
	##  Convert.query_to_option_apply_type("5", "arr") => "5"

	private
	def query_to_option_apply_type(val, type)
		case type
		when "int"; val.to_i
		when "flt"; val.to_f
		when "bol";
			downcased = val.downcase
			downcased == "true" or downcased == "on"
		when "sym"
			if val.empty? then
				:empty
			else
				val.to_sym
			end
		else val end
	end

	## This function is used by option_to_query. It does not handle arrays,
	## because arrays are taken care of in option_to_query.
	##  Convert.query_to_option_apply_type(5, "int")    => "5"
	##  Convert.query_to_option_apply_type("on", "bol") => "true"

	private
	def option_to_query_apply_type(val, type)
		case type
		when "bol";
			case val
			when TrueClass, FalseClass
				val ? "true" : "false"
			when String
				downcased = val.downcase
				(downcased == "true" or downcased == "on") ? "true" : "false"
			end
		else val.to_s end
	end

	public
end
