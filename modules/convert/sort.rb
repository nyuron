## The Convert module is a collection of functions that convert data from one format to another.
module Convert
	extend Convert
	## these keywords can be used in the sort method
	VALID_SORT_TOKENS = [:prio, :value, :key, :time, :type]

	## Generates ruby code out of a list of keywords, which represent the sorting sequence.
	## The list of keywords may be an array of (strings or symbols) or a string
	## with keywords seperated by commas and optional spaces.
	## a ! behind a keyword means negation
	##
	##  Convert.sort_sequence_to_code('prio,!value')
	##  # or
	##  Convert.sort_sequence_to_code(%w(prio value))
	##  # or
	##  Convert.sort_sequence_to_code([:prio, :value])
	##  # return:
	##
	##  class Neuron
	##  	def <=>(other)
	##  		if (test = (other.prio <=> @prio)) != 0
	##  			return +test
	##  		elsif (test = (other.value <=> @value)) != 0
	##  			return -test
	##  		else
	##  			0
	##  		end
	##  	end
	##  end
	def sort_sequence_to_code(seq)
		if String === seq
			seq = seq.split(/\s*,\s*/)
		elsif not Array === seq
			seq = []
		end


		code = "def <=>(other)\n"
		for token in seq
			code << sort_code(token)
		end
		code << "return 0 end"
	end

	def sort_code(token)
		token = token.to_s
		if token[0] == ?!
			token.slice!(0)
			sign, negsign = '-', ''
		else
			sign, negsign = '', '-'
		end

		case token.to_sym
		when :prio; <<DONE
	if (test = (other.prio <=> @prio)) != 0
		return #{sign}test
	end
DONE
		when :type; <<DONE
	if (test = (@type <=> other.type)) != 0
		return #{sign}test
	end
DONE
		when :key; <<DONE
	unless tag? or other.tag?
		if (test = (@key <=> other.key)) != 0
			return #{sign}test
		end
	end
DONE
		when :value; <<DONE
	if tag? or other.tag?
		if 0 != test = other.sortstring.to_i <=> @sortstring.to_i
			return #{sign}test
		end
	else
		if (test = (@sortstring <=> other.sortstring)) != 0
			return #{sign}test
		end
	end
DONE
		when :time; <<DONE
	if @time
		if other_time = other.time
			if (test = (@time <=> other_time)) != 0
				if @time < $now
					return (other_time < $now) ? #{negsign}test : #{sign}1
				else
					return (other_time >= $now) ? #{sign}test : #{negsign}1
				end
			end
		else
			return #{sign}1
		end
	else
		if other.time?
			return #{negsign}1
		end
	end
DONE
		else "" end
	end
end
