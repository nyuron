module Convert
	extend Convert

	## filter_to_sql is used to convert a fitler from user input format to
	## a format which SQLite3 accepts as a WHERE expression.
	##
	##  Convert.filter_to_sql("tag1, tag2") => "tags GLOB '*tag1*' AND tags GLOB '*tag2*'"
	##  Convert.filter_to_sql("a&b|c")      => "tags GLOB '*a*' AND tags GLOB '*b*' OR tags GLOB '*c*'"
	##  Convert.filter_to_sql("/foo/")      => "value GLOB '*foo*'"
	##  Convert.filter_to_sql("<9, >3")     => "prio < 9 AND prio > 3"
	def filter_to_sql(str)
		if str.nil?
			return ""
		end
		str = str.gsub(/(@?\w+)\b|\/.*?\/|[<>=]=?[\d.]+\b/) do |s|
			if s[0] == ?/
				"value REGEXP \"#{s[1..-2].sql}\""
#				"value GLOB \"*#{s[1..-2].sql}*\""
			elsif s[0] == ?@
				type = s[1..-1]
				if type == 'time'
					"type='txt' AND NOT info1=''"
				else
					"type='#{s[1..-1]}'"
				end
			elsif s[0] == ?<
				if s[1] == ?=
					"prio <= #{s[2..-1]}"
				else
					"prio < #{s[1..-1]}"
				end
			elsif s[0] == ?>
				if s[1] == ?=
					"prio >= #{s[2..-1]}"
				else
					"prio > #{s[1..-1]}"
				end
			elsif s[0] == ?=
				if s[1] == ?=
					"prio = #{s[2..-1]}"
				else
					"prio = #{s[1..-1]}"
				end
			else
				"tags GLOB '*#{TAG_SPLIT_STRING}#{s}#{TAG_SPLIT_STRING}*'"
			end
		end
		return str.gsubalot(
			['|', ' OR '],
			['!', ' NOT '], 
			['&', ' AND '],
			[',', ' AND '],
			['^', ' XOR ']
		)
	end

	## scans the filter for tags which may be hidden on the screen. Example:
	##  your filter: a & b & c
	##  the tags of a neuron: a, b, c, d, e
	##  because EVERY neuron you see will have at least the tags a, b and c,
	##  it's safe to hide them for a cleaner output.
	def filter_to_tags(filter)
		return [] if filter =~ /[!^|<>=\/]/
		tags = filter.strip.split(/\s*[,&]\s*/)
		return tags
	end
end

