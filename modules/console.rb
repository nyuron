module CLI end
module Console
	extend Console
	extend CLI

	VALID_CONSOLE_MODES = [:console, :search, :searchback]
	SEARCH_MODES = [:search, :searchback]
	PROMPT = {:console => ':', :search => '/', :searchback => '?'}

	def last_search_mode
		@@last_search_mode
	end
	def get_history
		@@history
	end

	def self.initialize()
		@@tabfreeze = false
		@@tabindex = 0
		@@historyfreeze = false
		@@history = {}
		for mode in VALID_CONSOLE_MODES
			@@history[mode] = []
		end
		@@historywords = nil
		@@historyindex = 0

		@@mode = nil
		@@next = nil
		@@str = ''
		@@info = ''
		@@infotime = Time.at(0)
		@@pos = 0
		@@last_search_mode = nil
	end

	## API {{{

	def validate_pos(wrapping = false)
		if @@str.size == 0
			@@pos = 0
		elsif @@pos < 0
			if wrapping
				@@pos += @@str.size + 1
				@@pos = 0 if @@pos < 0
			else
				@@pos = 0
			end
		elsif @@pos >= @@str.size
			@@pos = @@str.size - 1
		end
	end

	def len() @@str.size + 1 end
	
	def arrange_cursor()
		movi(UI.console_line, @@pos + 1)
	end

   def move_relative(n=0)
		@@tabfreeze = false
		@@historyfreeze = false

		newpos = @@pos + n
		return unless newpos.between? 0, @@str.size
      @@pos = newpos
		arrange_cursor
   end

   def move_absolute(n=0)
		@@tabfreeze = false
		@@historyfreeze = false

		@@pos = n
		validate_pos(true)
		arrange_cursor
   end

	def puts(pos, str)
		puti(UI.console_line, pos, str)
	end

	def insert(str, at=nil)
		@@tabfreeze = false
		@@historyfreeze = false

		at ||= @@pos
		@@str.insert(at, str)
		@@pos += str.size
		arrange_cursor
	end

	def delete(mod = 0)
		@@tabfreeze = false
		@@historyfreeze = false

		at = @@pos + mod
		@@str.slice!(at, 1)
		@@pos += mod
		arrange_cursor
#		if at < 0 or at >= @str.size
#			return
#		elsif at == @str.size - 1
#			put @str.size, ' '
#			@str.chop!
#		elsif at == 0
#			@str = @str[1..-1]
#		else
#			@str = @str[0...at] + @str[at+1..-1]
#		end
#		@pos = at
	end

	def empty?()
		@@str.empty?
	end

	##--------}}}

	def handle_key(c)
		case c
		when '<cr>'
			close
			cursor false
			clear
			history_add(@@str)
			case @@mode
			when :console
				Command.run(@@str)
			when :search
				API.scan(@@str, 1)
			when :searchback
				API.scan(@@str, -1)
			end
		when '<redraw>'; Info.request_redraw = true
		when '<esc>'; close
		when '<up>'; history :backward
		when '<down>'; history :forward
		when '<right>'; move_relative +1
		when '<left>';  move_relative -1
		when '<del>'; delete
		when '<tab>'; tabcomplete :forward
		when '<s-tab>'; tabcomplete :backward
		when '<end>'; move_absolute -1
		when '<home>'; move_absolute 0
		when '<c-u>'; 
			@@str = ''
			@@pos = 0
			arrange_cursor
		when '<bs>'
			close if empty?
			delete(-1)
		when ' '..'~'; insert(c)
		end
	end

	## valid modes: :console, :search, :searchback
	def main_loop(mode=:console)
		@@mode = mode
		draw
		API.busy false
		arrange_cursor
		key = geti
		API.busy true
		handle_key(key)
	end

	def open(str = '', mode=:console, pos = -1)
		Info.where = mode
		@@str = str
		@@pos = pos
		if SEARCH_MODES.include? mode
			@@last_search_mode = mode
		end
		validate_pos(true)
	end

	def raise(arg=nil)
		arg ||= $!
		case arg
		when Exception
			write "#{arg.class}: #{arg.message}"
		when String
			write "Error: #{arg}"
		else
			write "Unknown or unspecified error!"
		end
		logerr @@info
		for bt in Exception === arg ? arg.backtrace : caller(1)
			logerr bt
		end
		@error = true
	end
	alias error raise

	def write(string)
		@@info = string
		@@infotime = Time.now
	end

	def close()
		Info.where = :main
	end

	def time_left
		t = Query.get_time_left
		return unless t
		Convert.relative_time_to_string(t)
	end

	def clear
		attr_set(*Color.console.base)
		clear_content if !@info and Time.now - @@infotime > 10
		if @@info.empty?
			txt = "#{Opt.filter}   "
			txt << "brain[#{Query.db_size}] viewing[#{UI.list.size}]  "
			txt << "left[#{time_left}]"
			filter = " #{$now.strftime("%A %b %d#{$now.day.suffix}, %H:%M")} "
			txt = txt.ljust(cols-filter.size)[0..cols]
			txt << filter
			puts 0, txt.ljust(cols-1)[0..cols-1]

			attr_at(cols-filter.size+1, UI.console_line, -1, *Color.console.date)
			attr_at(0, UI.console_line, Opt.filter.size, *Color.console.tags)
#			Ncurses.refresh
#			sleep 1
#			color_bold_at(UI.console_line, 0, Opt.filter.size, -1, -1)
		else
			if @error
				attr_set(*Color.console.error)
			else
				attr_set(*Color.console.info)
			end
			txt = @@info
			puts 0, txt.ljust(cols-1)[0..cols-1]
		end
		attr_set(*Color.console.base)
	end

	def clear_content
		@@info.clear!
		@error = false
	end

	def draw
		attr_set(*Color.console.base)
		rest = cols - @@str.size - 2
		
		str = (PROMPT[@@mode] + @@str + ' ' * rest)
		if str.size > cols - 1
			str = str[str.size-cols+1 .. -1]
		end
		puts 0, str
		arrange_cursor
	end

	def tabcomplete(direction = :forward)
		if @@tabfreeze
			if direction == :forward
				@@tabindex += 1
				test = @@tabwords[@@tabindex]
				if not test
					@@tabindex = 0
				end
			elsif direction == :backward
				@@tabindex -= 1
				if @@tabindex < 0
					@@tabindex = @@tabwords.size - 1
#					if @@tabwords.respond_to?(:size)
#						@@tabindex = @@tabwords.size - 1
#					else
#						size = 0
#						for item in @@tabwords
#							size += 1
#							break if size > 30
#						end
#						@@tabindex = size - 1
#					end
				end
			else
				return
			end

			test = @@tabwords[@@tabindex]
			if test
				@@str[@@tabfreeze...@@tabend] = test
				@@pos = @@tabfreeze + test.size
				@@tabend = @@pos
			end
			
		else
			string = @@str[0...@@pos]
			words = string.split(' ')
			words << '' if @@str[@@pos-1] == ?\s
			if words.size == 1
				@@tabwords = Completer.new(words.first, Cache.cmd.__table__.keys)
			else
				@@tabwords = Command.tabcomp(string, @@pos, words.size - 1)
			end
			@@tabwordsize = (words.last.size rescue 0)
			@@tabfreeze = @@pos - @@tabwordsize
			@@tabend = @@pos
			@@tabindex = 0
			tabcomplete
		end
	end

	def history_add(str)
		unless @@str.empty? or @@history[@@mode].last == @@str
			@@history[@@mode].unshift @@str
			@@history[@@mode].pop if @@history[@@mode].size > (Opt.history_size || 30)
		end
		@@historyfreeze = false
	end

	def history(dir)
		unless @@historyfreeze
			@@historyfreeze = true
			@@historywords = Completer.new(@@str, @@history[@@mode])
			@@historyindex = 0
		end

		if dir == :backward
			@@historyindex += 1 unless @@historyindex >= @@historywords.size - 1
		elsif dir == :forward
			@@historyindex -= 1 unless @@historyindex <= 0
		else
			return
		end

		test = @@historywords[@@historyindex]
		if test
			@@str = test.dup
			@@pos = @@str.size
		end
	end
end

