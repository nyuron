#!/usr/bin/env ruby
#---------------------------------------
# This script prints the timestamp of
# the next important date, if it exists.
# otherwise, it prints 0

require 'sqlite3'
require 'pathname'
$: << MYDIR = File.dirname(Pathname.new(__FILE__).realpath)

require 'modules/boot'

fname = Boot.get_conf_path
dbfile = nil
load fname if fname
dbfile = Boot.get_database_path(dbfile)

abort 'no database found...' unless dbfile

db = SQLite3::Database.new(dbfile)

query = <<pines
SELECT info1 FROM nyu
WHERE type = 'txt' AND info1 > ?  AND tags GLOB '*/i/*'
ORDER BY info1 ASC;
pines

neurons = []
row = db.get_first_row(query, Time.now.to_i)
if row
	puts row.first
else
	puts 0
end

