#!/usr/bin/env ruby
#---------------------------------------
# This script generates a plain text overview
# of important notes for integration in other
# applications such as window managers.

require 'sqlite3'
require 'pathname'
$: << MYDIR = File.dirname(Pathname.new(__FILE__).realpath)

require 'modules/boot'
require 'modules/convert/time'

class Neuron
	attr_reader :time
	def initialize(row)
		@value, @time = row
		@time = Time.at(@time.to_i)
	end
	def <=>(other)
		@time <=> other.time
	end
	def to_s
		ts = Convert.time_to_string(@time).rjust(17)
		return "#{ts}: #@value"
	end
end

fname = Boot.get_conf_path
dbfile = nil
load fname if fname
dbfile = Boot.get_database_path(dbfile)

abort 'no database found...' unless dbfile


$now = Time.now
db = SQLite3::Database.new(dbfile)

query = <<X
SELECT value, info1
FROM nyu
WHERE type='txt'
AND NOT info1=''
AND info1>#{$now.to_i}
AND tags GLOB '*/i/*'
ORDER BY info1
X

neurons = []
db.execute(query) do |row| neurons << Neuron.new(row) end
unless neurons.empty?
	t = neurons.first.time
	puts "Time left: #{Convert.relative_time_to_string(t)}"
	neurons.each do |nyu| puts nyu end
end

puts
system 'cal'

